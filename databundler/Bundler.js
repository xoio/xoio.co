var UglifyJS = require('uglify-js');
var fs = require('fs');
var mode = "dev";
var args = process.argv.slice(2);

/**
 * Builds libs.
 * @param compress if compress is not undefined, will minify libs
 */
function buildLibs(libs,_compress){
    if(_compress === undefined){
        _compress = false;
    }

    var fileContents = [];

    // if we don't define a list of files, just get everything under
    // libs folder
    if(libs === undefined){
        libs = fs.readdirSync(__dirname + "/libs");

        //build absolute paths to libs
        libs = libs.map(function(item){
            return __dirname + "/libs/" + item;
        });
    }

    /**
     * If compress is false, we just want to concat libs
     */
    if(_compress === false || _compress === undefined){

        libs.forEach(function(item){
            var contents = fs.readFileSync(item,"utf8");
            fileContents.push(contents);
        });

        fs.writeFile(__dirname + "/resources/public/js/libs.js",fileContents.join("\n"),function(err){
            if(err){
                console.log("problem writing file : ",err);
            }
        })
    }else{
        var result = UglifyJS.minify(libs,{
            mangle:false,
            beautify:true
        })

        fs.writeFile(__dirname + "/resources/public/js/libs.min.js",result.code,function(err){
            if(err){
                console.log("problem writing file");
            }
        })
    }




}

var libs = [
    __dirname + "/libs/Three.js",
    __dirname + "/libs/ThreeCSS3D.js",
    __dirname + "/libs/Page.js",
    __dirname + "/libs/preload.js",
    __dirname + "/libs/tweenmax/TweenMax.min.js"
]


/**
 * Run the appropriate set of commands based on script args.
 * Default is dev
 */
switch (args[0]){
    case "dev":
        buildLibs(libs)
        break;


    case "prod":
        buildLibs(libs,true)
        break;

    default:
        buildLibs(libs);
        break;
}
