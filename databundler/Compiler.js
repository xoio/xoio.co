/**
 * A simple compilation script to parse content for the site into useable
 * Javascript objects for the front-end.
 * @type {Gulp|exports|module.exports}
 */


var fs = require("fs");
var marked = require("marked");
var jsedn = require("jsedn");

/**
 * Parses out the contents of the projects folder to build out a data structure
 * that represents all of the projects to be displayed on the front end.
 * @returns {string} returns a string representing the data structure to be written to a file that'll be used on the front end.
 */
function buildProjectContent(){

    //path to projects
    const basepath = __dirname + "/../resources/content/projects";

    //read projects folder
    var projects = fs.readdirSync(basepath);

    /**
     * Loops through projects and parse out content into new array
     */
    var compiledProjects = [];

    for(var i = 0; i < projects.length;++i){

        var compiledProject = {};

        var projectpath = basepath + "/" + projects[i];

        if(projectpath.search(".DS_Store") === -1){
            var project = fs.readdirSync(projectpath);

            //read the meta file
            var meta = JSON.parse(fs.readFileSync(projectpath + "/meta.json","utf8"));

            //read images folder
            var images = fs.readdirSync(projectpath + "/images");
            var imagePaths = [];
            for(var a = 0; a<images.length; ++a){
                if(images[a].search("DS_Store") === -1){
                    imagePaths.push("/content/projects/" + projects[i] + "/images/" + images[a]);
                }
            }

            if(meta.hasOwnProperty('client')){
                compiledProject["client"] = meta.client;
            }
            compiledProject["safename"] = meta.safename;
            compiledProject["title"] = meta.name;
            compiledProject["thumb"] = meta.thumb;
            compiledProject["images"] = imagePaths;
            compiledProject["content"] = marked(fs.readFileSync(__dirname + "/../resources/content/projects/" + projects[i] + "/content.md","utf8"));

            compiledProjects.push(compiledProject);
        }
    }

    return "(ns xoio.data) \n\n ;; Compiled project content for the site using node.js \n\n (def data (str " + jsedn.encode(compiledProjects) + "))\n";


} // end buildProjectContent


/**
 * Does the same thing as buildProjectContent but prepares all of the content in the "content" folder instead
 * which contains markdown files for site copy
 * @returns {string} a string representing
 */
function buildSiteContent(){
    //read the content directory
    var contentdir = fs.readdirSync(__dirname + "/../content");
    var contentlen = contentdir.length;

    var contents = {};

    for(var i = 0; i < contentlen;++i){

        //build path
        var file = __dirname + "/content/" + contentdir[i];

        //convert to html
        var mdfile = marked(fs.readFileSync(file,"utf8"));

        //build a key name based on filename
        contents[contentdir[i].replace(".md","").toLowerCase()] = mdfile;

    }


    return "\n window.PAGE_DATA=" + JSON.stringify(contents);
}

function compileExperiments(){
    var experiment = [];
    for(var i in experiments_list){
        experiment.push(experiments_list[i]);
    }

    return "\n window.EXPERIMENTS=" + JSON.stringify(experiment);
}

////////// START SCRIPT CODE ///////////////

//const datapath = __dirname + "/resources/public/js/Data.js";
const datapath = __dirname + "/../src/cljs/xoio/data.cljs";
//build our project content
var project_content = buildProjectContent();

//build out copy for the site
//var page_content = buildSiteContent();

//var experiments = compileExperiments();

//check if we have a Data.js file
if(fs.existsSync(datapath)) {
    //erase it, it's easier to just erase and re-create.
    fs.unlinkSync(datapath);
}

//write the file
fs.writeFile(datapath,project_content,function(err){
    if(err){
        console.log(err);
    }
});
