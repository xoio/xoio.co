(defproject xoio "0.1.0-SNAPSHOT"
  :description "website for xoio.co"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[hipo "0.5.1"]
                 [secretary "1.2.3"]
                 [garden "1.3.0-SNAPSHOT"]
                 [thi.ng/color "1.0.0"]
                 [thi.ng/geom "0.0.908"]
                 [thi.ng/domus "0.2.0"]
                 [org.clojure/tools.reader "1.0.0-alpha2"]
                 [org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "1.7.170"]
                 [figwheel-sidecar "0.5.0"]
                 [org.clojure/core.async "0.2.374"]]

)
