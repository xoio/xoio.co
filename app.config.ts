import { defineConfig } from "@solidjs/start/config";
import UnoCSS from "unocss/vite";
import postcssNesting from "postcss-nesting"

import * as path from "path"

//@ts-ignore
import postcssMixins from "postcss-mixins"

export default defineConfig({
	vite: {
		plugins: [UnoCSS()],
		css: {
			postcss: {
				plugins: [
					postcssNesting,
					postcssMixins
				] 
			}
		},

		resolve: {
			alias: {
				"@comp": path.resolve("./", "src/components"),
				"@wgl":path.resolve("./","src/lib/gl")

			}
		}
	}
});
