
export type ImageItem = {

	id: string,
	url: string,
}

export type ProjectAssets = {

	id: string
	mainImage: ImageItem,
	assets: Array<ImageItem>
}

export type SanityItem = {

	_createdAt: string,
	_id: string,
	_rev: string,
	_type: string,
	_updatedAt: string,
	body: string,
	mainImage: any,
	photos: Array<any>,
	slug: string,
	title: string
}