import { createQuery, useQueryClient } from "@tanstack/solid-query"
import { createSignal, createEffect, onMount, Accessor, createResource, createContext, useContext } from "solid-js"
import { CONTENT_OPTS } from "~/lib/constants"
import { getContent } from "~/server/sanity"
import { ImageItem, SanityItem } from "~/types"


type Project = {
	id: string
	pctLoaded: number
	doneLoading: boolean
	assets: Array<ImageItem>
}

type SiteContent = {
	thumbnailsLoaded: boolean
	projects: Array<Project>
}

export const ContentCtx = createContext()

export function useContent() {
	return useContext(ContentCtx)
}

// A component mainly responsible for initial content loading. It will 
// - kick off loading the thumbnails. Thumbnails will be loaded before the site content gets shown. 
// - It will also start to load any other additional assets which may or may not be done by the time you read the signals 
// being passed down through the context. 
//
// The context provides two signals, one just holds the site content and whether or not the thumbnails have been loaded. 
// The other will hold the current percentage of thumbnails loaded. 
function Content(props: any) {

	const [projects, setProjects] = createSignal<Array<Project>>([])

	const [site_content, set_content] = createSignal<SiteContent>({
		projects: [],
		thumbnailsLoaded: false
	})

	const [thumbnailPct, setThumbnailPct] = createSignal(0)

	// get the data 
	const project_data = createQuery(() => (CONTENT_OPTS))

	createEffect(() => {

		// Once we have the urls built(see core.tsx), we can start to setup 
		// things to load the thumbnails. We want to load thumbnails first before showing
		// the site 
		if (project_data.isSuccess && !site_content().thumbnailsLoaded) {

			loadThumbnails()
		}


	})

	function loadProjectAssets() {

	}

	// Loads a project's thumbnails 
	function loadThumbnails() {

		const projects: any = []

		const thumbs = project_data.data.map((itm: any) => {
			return {
				id: itm._id,
				url: itm.mainImage.small
			}
		})

		let loaded = 0
		let total = thumbs.length

		let thumb_prom = thumbs.map((thumb: any) => {
			return new Promise((res, rej) => {
				let img = new Image()
				img.src = thumb.url
				img.onload = () => {
					loaded += 1
					// update load percentage of thumbnails 
					let loadPct = Math.floor((loaded / total) * 100);

					setThumbnailPct(prev => {
						let tmp = prev
						tmp = loadPct
						return tmp
					})

					res({
						id: thumb.id,
						img
					})
				}
			})
		})

		Promise.all(thumb_prom).then(data => {

			// look through the project data to see where this image should be 
			for (let i = 0; i < project_data.data.length; ++i) {


				for (let j = 0; j < data.length; ++j) {

					if (data[j].id === project_data.data[i]._id) {

						projects.push({
							...project_data.data[i],
							thumbnail: data[j].img
						})
						break
					}
				}
			}

			set_content(prev => {
				let tmp = { ...prev }
				tmp.thumbnailsLoaded = true
				tmp.projects = projects
				console.log(tmp)
				return tmp
			})
		})
	}

	return (
		<ContentCtx.Provider value={[site_content, thumbnailPct]}>
			{props.children}
		</ContentCtx.Provider>
	)

}

export default Content 