import { useNavigate } from "@solidjs/router";
function Header() {
  const navigate = useNavigate();

  function onClick(e: Event) {
    navigate("/", { replace: true });
  }

  return (
    <header>
      <h1 class="font-size-5 font-bold" onclick={onClick}>xoio</h1>
    </header>
  );
}

export default Header;