(ns xoio.app
  (:require
    [components.panel]
    [thi.ng.domus.core :as dom]
    [xoio.content :as content]
    [xoio.globals :as globals]
    [routes.home :as home]
    [routes.project :as projects]))

(defn onLoad []
  (globals/prepareGlobalListeners)
  (globals/prepareMenu)
  (dom/add-class! js/document.body "show")
  (dom/add-class! globals/header "show")
  (dom/add-class! globals/site "show"))


(defn init []

  (try
    (js/Typekit.load
      (clj->js {:loading (fn[] ())
                :active
                  (fn[]
                    (content/loadContent
                      (fn []
                        (onLoad)

                        ;; setup routing for home
                        (js/page (:path home/route) (:enter home/route))
                        (js/page.exit (:path home/route) (:exit home/route))

                        ;; setup routing for projects
                        (js/page (:path projects/route) (:enter projects/route))
                        (js/page.exit (:path projects/route) (:exit projects/route))


                        (.start js/page)
                  ))


                                         )

                               }))
    (catch :default e
      (js/console.error e)))

  )


