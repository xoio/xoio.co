(ns xoio.content
  (:require
    [cljs.tools.reader :as edn]
    [xoio.data :as raw]
    [xoio.loader :as loader]))
;; read our data
(def data (edn/read-string raw/data))

;; keep a count of all the images we're loading
(def num 0)


(defn loadContent [callback]
  "Loads our project content"
  ;; start parsing out our data to extract all of the images we need to preload
  (let [payload #js []]
    ;; loop all the data
    (doseq [dat data]
      ;; get images array
      (let [images (get dat "images")]

        ;; loop image array
        (doseq [img images]
          ;; build a payload item for preloadjs and add to preload array
          (let [item {:project (get dat "safename")
                      :src img
                      :client (get dat "client")
                      :title (get dat "title")
                      :id num}]
               (.push payload (clj->js item)))
          ;;increment the image count
          (set! num (inc num)))
      ))
      (loader/loadManifest payload callback)))
