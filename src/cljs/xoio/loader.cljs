(ns xoio.loader
  (:require
    [xoio.globals :as globals]
    [goog.object :as obj]
    [framework.styles :as styles]
    [xoio.globals :as globals]))

(def ^:export queue (js/createjs.LoadQueue.))
(def ^:export loader (js/document.querySelector "#LOADER"))


(defn handleComplete [e]
  (js/console.log "done")
  (styles/addStyle loader "hide")
  (globals/assignImageToProject))


(defn handleFileLoaded [e]
"Handles things once a file has loaded"
  (let [item (.-item e)
        img (.-result e)]
    (let [project {:safename (aget item "project")
                   :src img}]
      (.push globals/projectImages project))))

(defn handleError [e]
"handles things when theres a error"
  (println "Error in loading manifest" e))

(defn handleProgress [e]
  "Handles progress for the file loading"
    (set! (.-style.width (js/document.querySelector "#LOADER")) (str (* (aget e "progress") 100) "%")))

;; setup listeners for everything
(.on queue "fileload" handleFileLoaded)
(.on queue "error" handleError)
(.on queue "complete" handleComplete)
(.on queue "progress" handleProgress)

(defn loadManifest
  ([manifest]
  "Runs a manifest through preloadjs"
  (.loadManifest queue manifest))

  ([manifest callback]
    "Runs a manifest through preloadjs and attaches a additional callback that
    runs when the manifest is done loading"
    ;; add an additional listener for the complete event
    (.on queue "complete" callback)
    (.loadManifest queue manifest)
    ))
