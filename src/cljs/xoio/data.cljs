(ns xoio.data) 

 ;; Compiled project content for the site using node.js 

 (def data (str [{"client" "Google" "safename" "frightgeist" "title" "FrightGeist" "thumb" "/content/projects/frightgeist/images/1.jpg" "images" ["/content/projects/frightgeist/images/1.jpg" "/content/projects/frightgeist/images/2.jpg" "/content/projects/frightgeist/images/3.jpg" "/content/projects/frightgeist/images/4.jpg"] "content" "<div class=\"intro project-copy\">
<strong>Introduction</strong>
<br/>

I was brought onto this project by the good folks at <a href=\"http://www.useallfive.com/\">UseAllFive</a> to help out with some of the WebGL work 
necessary to help bring the map to life. I helped build out the foundational portions of the map including setting up the geometry and meshes to 
create the particle system that the map comprises, in addition to doing additional work in integrating the various other components necessary
to get the map to react the way it does. 

<br/>
In addition to all of that I build out a couple of other smaller visual things like the dripping effect on a costume page as well as the 
spider web that appears on the state summary pages. 
</div>

<div class=\"technicals project-copy\">

<strong>Technicals</strong>
<br/>

    The work done was fairly straightforward. For the map I explored a couple of different approaches; at first I made a attempt to integrate
    a physics engine but soon realized that, given the size of the dataset, that having a engine as well as keeping things performant
    would soon prove to be a extreme challenge. That being said we settled on a much simpler particle system integrating Jono Brandel&#39;s port
    of the <a href=\"http://jonobr1.github.io/Physics/\">Traer physics&#39;s library</a> to help build out the animation. 
    <br/>
    <br/>
    The particles are built out of complete meshes, with shared geometry and materials as opposed to simple points as one might normally do with a 
    particle system. While using points did indeed work fine in terms of visual appearance, we soon discovered, that, unfortunately, in spite of visually
    appearing the correct size, that the hit area for Raycasting was sadly, stuck to the size of the point which is roughly 1 pixel, increasing the pixel 
    size in the vertex shader oddly didn&#39;t appear to help :( 
    <br/>
    <br/>
    Moving on, for the drips, I turned to another one of Jono&#39;s projects <a href=\"https://jonobr1.github.io/two.js/\">Two.js</a> as I am terrible at ploting out
    bezier curves and wanted something that could look nice at any resolution. To get the curves, I read a existing svg path into the Two.js instance I created, 
    and from there, was able to manipulate the path like I would vertices in Three.js.
    <br/>
    <br/>
    For the spiderweb, I ported a example from <a href=\"https://github.com/subprotocol/verlet-js\">Verlet-js</a> as it basically already did everything we needed it to do. 
    For the most part, the sample is exactly the same, but re-built a bit in order to allow it to resize and be responsive. The library was also modified a bit in order
    to remove some of the drawing commands as well as enable the ability to choose colors a bit eaiser.
</div>"} {"client" "Microsoft" "safename" "microsoft" "title" "Halo 5 : Guardians" "thumb" "/content/projects/microsoft/images/1.jpg" "images" ["/content/projects/halo5/images/copsredacted.jpg"] "content" "<div class=\"intro project-copy\">
<strong>Introduction</strong>
<br/>

Around the start of the summer, I was asked to participate in the development of a interactive experience for Microsoft. This would have been a experience for the 
recently release video game, Halo 5 : Guardians. I was part of a team that put together a rpg/choose your own adventure-like game. I won&#39;t spoil any of the story, but more 
or less, the game imparted some of the backstory surrouding the events leading up to the Halo 5 story.

Sadly though, within a couple of weeks of the scheduled launch, the project got axed (◕︵◕)
</div>

<div class=\"technicals project-copy\">

<strong>Technicals</strong>
<br/>
The bulk of the site is comprised of a custom framework utilizing Twitter&#39;s FlightJs. I helped put together a couple components 
that governed the UI as well as integration of audio and other misc. things. I also wrote the initial draft for the console tool 
in the main game itself. 

In addition to all of that, I got to do a little WebGL/Canvas2D work (which was handled using Three.js and Pixi.js) related work by writing a GLSL shader or two that was integrated into the 
main story.

</div>"} {"client" "Google" "safename" "nexus" "title" "Google Nexus 5" "thumb" "/content/projects/nexus/images/1.jpg" "images" ["/content/projects/nexus/images/1.jpg" "/content/projects/nexus/images/2.jpg" "/content/projects/nexus/images/3.jpg" "/content/projects/nexus/images/4.jpg"] "content" "<div class=\"intro project-copy\">
<strong>Introduction</strong>
<br/>
This was a project done while working at Unit9. The San Francisco branch had a google account
and was tasked with finishing up the new Nexus Website.
</div>

<div class=\"technicals project-copy\">

<strong>Technicals</strong>
<br/>
This was a basically a pretty straightforward Angular.js website detailing the new Nexus 5 as well as
featuring the Nexus 10 and 7.
<br/>
<br/>

I was responsible primarily with coming up with a pseudo media player(in that it really only 
played one video) as well as later on moving onto dealing with Localization of the site
which ended up being translated into well over 15 languages.
</div>"}]))
