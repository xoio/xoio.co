(ns xoio.globals
  (:require
    [xoio.data :as raw]
    [framework.page :as page]
    [framework.capabilities :as canuse]
    [thi.ng.domus.core :as dom]
    [cljs.tools.reader :as edn]))

;; read our raw data
(def data (edn/read-string raw/data))

;; global elements that will be necessary in various parts of the site
(def site (.querySelector js/document "#SITE"))
(def app (.querySelector js/document "#SITE_CONTENT"))
(def header (.querySelector js/document "#HEADER"))
(def menu (.querySelector js/document "#MENU"))

;; global store of all of the image media for the site
(def projectImages #js [])

;; global store of all of the project data on the site
(def projects #js [])

;; are we showing the menu?
(def menuVisible false)

(defn getProject [name]
  "Quick way of searching through project cache and getting data"
  (let [returnProject (atom 0)]
    (doseq [project projects]
      (let [safename (aget project "safename")]
        (if (= name safename)
          (swap! returnProject :val project)
          )))
      (deref returnProject)
    ))

;;;;;;;;;;;;;;;;;;;;;;;;;;; FUNCTIONS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn assignImageToProject []
  "Called after all of the images are loaded - iterates through the content data and associates all
   of the images to their it's respective project data and creates objects about each project
   for easy referencing later. "
  (doseq [dat data]
    (let [projectTmp #js []
          projectName (get dat "safename")]
      (doseq [img projectImages]
        (let [imgname (:safename img)]
          (if (= imgname projectName)
            (.push projectTmp (:src img)))))

        (.push projects
               (clj->js {:title (get dat "title")
                         :client (get dat "client")
                         :safename (get dat "safename")
                         :content (get dat "content")
                         :images projectTmp}))))

  )



(defn toggleMenu []
  "Toggles the opening and closing of the menu"
  (if (= menuVisible false)
    (do
      (js/TweenMax.to menu 1.2 (clj->js {:left 0
                                         :ease js/Power4.easeInOut}))
      (set! menuVisible true))
    (do
      (js/TweenMax.to menu 0.8 (clj->js {:left "-100%"
                                         :ease js/Power4.easeInOut}))
      (set! menuVisible false))

    )
  )


(defn prepareMenu []
  "Build all of the necessary components to make the menu functional"
  (if (= canuse/hasWebGL true)
    (do

      ))

  ;; setup menu listeners
  (let [nav (-> (.getElementsByClassName js/document "menu-link") (page/nodeToJSArray))]
    (doseq [navItem nav]
      (.addEventListener navItem "click"
                         (fn [e]
                           (let [target (aget e "target")
                                 external (.getAttribute target "data-type")
                                 url (.getAttribute target "data-url")]
                             (if (not= external nil)
                               (.open js/window url "target=_blank")
                               (do
                                 (toggleMenu)
                                 (js/page url)))))))
    )
  )



(defn prepareGlobalListeners []
  "Prepare any listeners that span the site"
  (.addEventListener header "click"
                     (fn []
                       (toggleMenu)))

  )
