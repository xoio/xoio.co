(ns routes.project
  (:require
    [framework.capabilities :as capabilities]
    [components.panel :as panel]
    [thi.ng.domus.core :as dom]
    [xoio.globals :as globals]
    [framework.page :as page]))


(defn adjustContentPanel []
  "Makes sure that the content panel stays within the bounds of the main panel"
  (let [content (.querySelector js/document "#PROJECTS")]
    (dom/set-style! content {:width (str (panel/calculatePanelChildWidth) "px")})))


(defn tweenInPanel []
  "Tween in the content panel"
  (let [projects (.querySelector js/document "#PROJECTS")]
    (.to js/TweenMax projects 1.2 (clj->js {:margin-top 0
                                            :ease js/Power4.easeInOut}))))

(defn tweenOutPanel []
  "Tween out the content panel"
  (let [projects (.querySelector js/document "#PROJECTS")]
    (.to js/TweenMax projects 1.2 (clj->js {:margin-top "120%"
                                            :ease js/Power4.easeInOut}))))


(defn enter [e]
  ;; append if not already there
  (if (= (.querySelector js/document "#PROJECTS") nil)

    (do
      (page/append globals/app [:div#PROJECTS
                                [:div.project-wrap
                                 [:div.project-contents
                                  [:div.project-title]
                                  [:div.project-copy-container]
                                  [:div.project-media]]]])

      (tweenInPanel)
      (.addEventListener js/window
        "resize"
        (fn []
          (adjustContentPanel)
          ))

      (let [project (aget (aget e "params") "id")
            projectItem (globals/getProject project)]

        (capabilities/whenMobile
          (fn [])
          (fn []
            (adjustContentPanel)

            ;; append title, client , etc.
            (let [title (page/getElementByClassName "project-title")
                  titleContent [:div
                                [:h2 (aget projectItem "title")]
                                [:h4 (aget projectItem "client")]
                                ]]
              (page/append title titleContent))

            ;; append copy
            (let [copy (page/getElementByClassName "project-copy-container")
                  copyContent (-> (.createElement js/document "div") (aset "innerHTML" (aget projectItem "content")))]
              (aset copy "innerHTML" (str copyContent copyContent copyContent copyContent))
              )


            )


          )


        )
      )

    ;; if the elements have already been laid out, just find/replace content
    (do

      (let [project (aget (aget e "params") "id")
            projectItem (globals/getProject project)
            titleDiv (page/getElementByClassName "project-title")
            title (page/getElementByTagName titleDiv "h2")
            client (page/getElementByTagName titleDiv "h4")]

          (aset title "innerHTML" (aget projectItem "title"))
          (aset client "innerHTML" (aget projectItem "client"))
        )

      (tweenInPanel)

      )




    )

  )


(defn exit [e next]
  (js/console.log "exiting")
  (tweenOutPanel)
  (next)
 )


;; describes the home route for the router
(def route
  {:path "/project/:id"
   :enter enter
   :exit exit})