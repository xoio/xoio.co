(ns routes.home
  (:require
    [framework.capabilities :as capabilities]
    [thi.ng.domus.core :as dom]
  [xoio.globals :as globals]
  [framework.page :as page]))

(def home [:div {:id "HOME"}])

(def showingThumbLinks (atom false))

(defn setMobileOrNot []
  "Simple function to add classname depending on mobile device or not"
  (if (= capabilities/isMobile true)
    (str "is-mobile")
    (str "not-mobile")))


(defn enter []
  (let [menu (-> (.getElementsByClassName js/document "project-item") (page/nodeToJSArray))]
    (doseq [menuItem menu]
      (dom/remove-class! menuItem "hide")))

  ;; populate home with project links
  (let [projects globals/projects]
    (doseq [project projects]
      ;; append titles
      (let [title [:div.project-item {:data-safename (aget project "safename")} [:h2 {:class (setMobileOrNot)} (.-title project)] ]]
        (set! home (conj home title)))

      ;; append content to project preview
      (let [projectImage (aget (aget project "images") 0)]
        (dom/set-attribs! projectImage {:data-name (aget project "safename")})
        (dom/add-class! projectImage (str "project-preview project-preview-" (aget project "safename")))
        (.appendChild (page/getElementByClassName "preview-wrap") projectImage))

      ))


  ;; append if not already there
  (if (= (.querySelector js/document "#HOME") nil)
    (do
      (page/append globals/app home)
      (capabilities/whenMobile
        (fn [])
        (fn []
          (let [menu (-> (.getElementsByClassName js/document "project-item") (page/nodeToJSArray))]
            (doseq [menuItem menu]
              (.addEventListener menuItem "click" (fn [e]
                                                    (let [target (aget e "target")
                                                          safename (.getAttribute (aget target "parentNode") "data-safename")]
                                                      (js/page (str "/project/" safename)))))))
          ))
      ))

  )


(defn exit [e next]
  (let [menu (-> (.getElementsByClassName js/document "project-item") (page/nodeToJSArray))]
    (doseq [menuItem menu]
      (dom/add-class! menuItem "hide")))

  (next)
  )


;; describes the home route for the router
(def route
  {:path "/"
   :enter enter
   :exit exit})