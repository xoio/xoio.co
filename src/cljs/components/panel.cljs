(ns components.panel
  (:require
    [thi.ng.domus.core :as dom]
    [xoio.globals :as globals]
    [framework.page :as page]))

;; padding for the main panel
(def padding 30)


(defn calculatePanelWidth []
  (let [currentWidth (.-innerWidth js/window)
        adjustedWidth (- currentWidth padding)]
    (str adjustedWidth "px")))

(defn calculatePanelChildWidth []
  "Calculates a full for child elements that enables it to still
  stay within the panel"
  (let [width (-> (calculatePanelWidth) (js/parseInt))]
    (- width (+ padding 10))))


(defn calculatePanelHeight []
  (let [currentHeight (.-innerHeight js/window)
        adjustedHeight (- currentHeight padding)]
    (str adjustedHeight "px")))


;; set the initial style of the panel
(dom/set-style! globals/site {:width (calculatePanelWidth)
                              :height (calculatePanelHeight)})

(.addEventListener
  js/window
  "resize"
  (fn []
    (let [main globals/site
          projects (.querySelector js/document "#PROJECTS")]
      (dom/set-style! main {:width (calculatePanelWidth)
                            :height (calculatePanelHeight)})
      (if (not= projects nil)
        (dom/set-style! projects {:height (str js/window.innerHeight "px")}))
      )))