(ns framework.page
  (:require
    [thi.ng.domus.core :as dom]
    [hipo.core :as hipo]))


(defn append [el what]
  "Appends content to the specified element while processing the
  vector through hipo"
  (.appendChild el (hipo/create what)))

(defn parseToHTML [what]
  "Parses the specified vector into html via hipo"
  (hipo/create what))

(defn nodeToJSArray [nArray]
  "Converts a HTML collection into a regular array"
  (let [arr #js []]
    (dotimes [i (.-length nArray)]
      (.push arr (aget nArray i)
             ))
    arr))


(defn getElementsByClassName
  ([classname]
   (let [list (.getElementsByClassName js/document classname)]
     (nodeToJSArray list)))

  ([el classname]
   (let [list (.getElementsByClassName el classname)]
     (nodeToJSArray list)))
  )


(defn getElement [id]
  "Wrapper around Thi.ng's (by-id) function"
  (dom/by-id id))

(defn getElementByClassName
  ([classname]
   (let [list (.getElementsByClassName js/document classname)]
     (let [convertList (nodeToJSArray list)]
       (aget convertList 0))))


  ([el classname]
   (let [list (.getElementsByClassName el classname)]
     (let [convertList (nodeToJSArray list)]
       (aget convertList 0))))

  )


(defn getElementByTagName
  ([classname]
   "Gets a set of elements via it's tag name from the entirety of the dom and returns
   the first result"
   (let [list (.getElementsByTagName js/document classname)]
     (let [convertList (nodeToJSArray list)]
       (aget convertList 0))))


  ([el classname]
   "Gets a set of elements via it's tag name from the specified element and returns
    the first result"
   (let [list (.getElementsByTagName el classname)]
     (let [convertList (nodeToJSArray list)]
       (aget convertList 0)))))