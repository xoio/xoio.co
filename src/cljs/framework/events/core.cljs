(ns framework.events.core)

(defn addListener [el & specs]
  "Adds a sequence of maps mapping to listner events onto a element
  'specs' can be any number of maps containing the keys
  1. event
  2. callback"
  (doseq [spec specs]
    (let [eventname (find spec :event)]
      (if (not= eventname nil)
        (do
          (.addEventListener el (:event spec) (:callback spec)))

        (do
          ())
        )
      )))

