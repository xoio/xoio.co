(ns framework.events.scroll
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require
    [framework.capabilities :as capabilities]
    [cljs.core.async :refer [<! put! chan alts! timeout]]
    [goog.events :as events]
    [goog.dom :as dom]
    [goog.events.EventType :as EventType]))


(def touchStartX 0)
(def touchStartY 0)
(def touchMult 2)
(def firefoxMult 15)
(def mouseMult 1)

(def event {:x 0
            :y 0
            :deltaX 0
            :deltaY 0
            :originalEvent nil})


(defn listen [el type]
  "Listener for funnaling all of the event type to get
  processed by core.async"
  (let [out (chan)]
    (events/listen
      el type
      (fn [e] (put! out e))) out))

(defn notify [e]
  (aset event :x (+ (:x event) (aget event :deltaX)))
  (aset event :y (+ (:y event) (aget event :deltaY)))
  (aset event :originalEvent e)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; setup events
(def scroll (listen js/window "wheel"))
(def touchStart (listen js/window "ontouchstart"))
(def touchMove (listen js/window "touchmove"))

(go
  (while true
    (let [[v c] (alts! [scroll touchStart touchMove])]

      ;; if we get the touchstart event, then set touchStart x and y right away
      (condp = c
        touchStart (let [e (.-event_ (<! c))]
                     (let [t (.-targetTouches e)]
                       (set! touchStartX (.-pageX t))
                       (set! touchStartY (.-pageY t)))))

      ;; otherwise manage the event like normal
      (let [e (.-event_ (<! c))]

        ;; we're gonna rely simply on knowning whether or not a device is mobile
        ;; in order to determine how to calculate values.
        ;; We don't utilize (condp) so that we don't have to write extra code
        (if (not= capabilities/isMobile true)

          ;; handle regular desktop event
          (let [deltaX (.-wheelDeltaX e)
                deltaY (.-wheelDeltaY e)]

            ;; if statement to check for firefox
            (if (= capabilities/isFirefox true)
              (do
                (aset event :deltaX (* firefoxMult deltaX))
                (aset event :deltaY (* firefoxMult deltaY)))
              (do
                (aset event :deltaX (* mouseMult deltaX))
                (aset event :deltaY (* mouseMult deltaY)))

              ))

          ;; handle mobile event
          (let [t (.-targetTouches e)]
            (let [deltaX (* (- (.-pageX t) touchStartX) touchMult)
                  deltaY (* (- (.-pageY t) touchStartY) touchMult)]
              (aset event :deltaX deltaX)
              (aset event :deltaY deltaY)
              (set! touchStartX (.-pageX t))
              (set! touchStartY (.-pageY t))
              ))

          )

        (notify e)
        )



      )
    ))