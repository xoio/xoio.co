(ns framework.protocols.displayobject
  (:require
     [framework.math :as math]
     [framework.utils :as styles]
     [thi.ng.geom.core :as geom]
     [hipo.core :as hipo]
     [thi.ng.geom.core.vector :as v :refer [vec2 vec3]]
     [thi.ng.domus.core :as dom]))



(defprotocol DisplayObject
  (init [this] "Initializes the object")
  (addListener [this listeners] "Adds a listener to the map of listeners to pay attention to")
  (appendTo [this el] "Appends the DisplayObject to the specified element(ideally passed in as a hipo object)"))


;; extend some of the declared methods of the DisplayObject protocol to do the same thing
;; across implementations
(extend-protocol DisplayObject
  object
  (init [this]
    "Initializes the DisplayObject"
    ;; setup attribs
    (aset this "attribs" {:style {:position "absolute"}})
    (aset this "listeners" {})

    (set! (.-attribs this) (merge (.-attribs this) (.-listeners this)))

    ;; build the DOM node
    (aset this "domElement" [:div (.-attribs this) "test"])

    ;; apply style attributes

    ;;setup some other things that could be used for animation
    (aset this "position" (vec3 0 0 0))
    (aset this "velocity" (vec3 0 0 0))
    (aset this "acceleration" (vec3 0 0 0))
    this)

  (addListener [this listeners]
    (set! (.-attribs this) (merge (.-attribs this) listeners))
    (set! (.-domElement this) (assoc (.-domElement this) 1 (.-attribs this))) this)

  (appendTo [this el]
    "Appends DisplayObject to specified element. If the specified element is a not a vector, then
    we assume it's a regular DOM node and first convert the Hipo markup to a regular node"
    (if (vector? el)
      (conj el (.-domElement this))
      (.appendChild el (hipo/create (.-domElement this)))
      ))
    )
