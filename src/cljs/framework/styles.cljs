(ns framework.styles)

(defn addStyle [el & classname]
  "Adds a list of classnames onto the element you pass in"
  (doseq [class classname]
    (.add (.-classList el) class)))


(defn removeStyle [el & classname]
  "Removes all of the specified classnames on the element you pass in"
  (doseq [class classname]
    (.remove (.-classList el) class)))
