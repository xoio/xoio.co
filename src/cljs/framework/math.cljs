(ns framework.math
  (:require
     [thi.ng.geom.core :as geom]
     [thi.ng.geom.core.vector :as v :refer [vec2 vec3]]))

(defn addScalar [vec scalar]
  "Adds the vector by a scalar value. Accepts either vec2 or vec3"
    (if (= (.-buf.length vec) 2)
      (let [b 0]
          (aset vec :x (+ (:x vec) scalar))
          (aset vec :y (+ (:y vec) scalar)))
      (let [b 1]
          (aset vec :x (+ (:x vec) scalar))
          (aset vec :y (+ (:y vec) scalar))
          (aset vec :z  (+ (:z vec) scalar)))
       ) vec)

(defn multiplyScalar [vec scalar]
  "Multiplies the vector by a scalar value. Accepts either vec2 or vec3"
  (if (= (.-buf.length vec) 2)
    (let [b 0]
      (aset vec :x (* (:x vec) scalar))
      (aset vec :y (* (:y vec) scalar)))
    (let [b 1]
      (aset vec :x (* (:x vec) scalar))
      (aset vec :y (* (:y vec) scalar))
      (aset vec :z  (* (:z vec) scalar)))
      ) vec)

;; General useful math equations
(defn lineLength [x y x0 y0]
  "Determines the length of a line"
  (let [x (* (- x x0) x)
        y (* (- y y0) y)]
    (js/Math.sqrt (+ x y))))

(defn randomRange [min max]
  "Returns a random value in a range"
  (* (+ min (js/Math.random.)) (- max min)))

(defn lerp [val min max]
  "Performs linear interpolation"
  (+ min (* (- max min) value)))

(defn norm [val min max]
  "Normalizes a value"
  (/ (- val min) (- max min)))
