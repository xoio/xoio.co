(ns framework.utils)

(defn getStyle[el]
  "Returns the style object for the specified element"
  (let [styleVal (.getComputedStyle js/window el)]
      styleVal))


(defn getStyleById[id]
  "Returns the style object for the specified element indicated by a id"
  (let [styleVal (.getComputedStyle js/window (js/document.querySelector id))]
      styleVal))


(defn getStyleProp [el prop]
  "Get the value for the specified property"
  (let [style (getStyle el)]
    (aget el prop)))
