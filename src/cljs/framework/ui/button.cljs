(ns framework.ui.button
  (:require
    [hipo.core :as hipo]
    [framework.protocols.displayobject :as DisplayObject]
    [framework.capabilities :as features]))

(defn AddListener [btn listenerMap]
  (DisplayObject/addListener btn listenerMap))

(deftype Button []
  framework.protocols.displayobject/DisplayObject)

(defn MakeButton []
  "Creates a new Button object"
  (let [base (Button.)]
    (DisplayObject/init base)))
