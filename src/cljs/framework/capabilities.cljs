(ns framework.capabilities
  (:require
    [thi.ng.domus.core :as dom]))

(def mobile false)
(def webgl false)
(def firefox false)
(def chrome false)


;;;;;;; Begin general functions ;;;;;;;;

(defn isMobile []
  "Helps to determine if we're on a mobile device"
  (let [regex (re-find #"Android|webOS|iPhone|iPad|iPod|BlackBerry" js/navigator.userAgent)]
    (if (not= regex nil)
      (set! mobile true))))


(defn hasWebGL []
  "Does a quick check to see if the user can use webgl.
  TODO - add in a better check to account for other possible context references"
  (let [canvas (.createElement js/document "canvas")
        ctx (.getContext canvas "webgl")]
    (if (= ctx nil)
      (false)
      (let [val true]
        (set! webgl true)
        val))))

;;;; begin deteection for specific browsers ;;;;;;

(defn isFirefox []
  "Special detection for Firefox"
  (let [ua (.-navigator.userAgent js/window)]
    (if (not= -1 (.indexOf ua "FireFox"))
      (do
        (set! firefox true)
        true)
      (let [val false]
        val))))

(defn isChrome []
  (let [ua (.-navigator.userAgent js/window)]
    (if (not= -1 (.indexOf ua "Chrome"))
      (do
        (set! chrome true)
        true)
      (let [val false]
        val))))



(isFirefox)
(isChrome)
(isMobile)
(hasWebGL)


(defn whenMobile [mfunc func]
  "Provides a easy way to write situational callback functions between mobile and
  desktop versions of a mechanism"
  (if (= mobile true)
    (mfunc)
    (func)))