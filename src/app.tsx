import "@unocss/reset/tailwind.css";
import "virtual:uno.css";

import { Router } from "@solidjs/router";
import { FileRoutes } from "@solidjs/start/router";
import { Suspense } from "solid-js";
import Header from "~/components/header";
import "~/styles/main.css";
import "~/styles/pages/home.css";
import Content from "./components/content"
import Loading from "./components/loading";
import Core from "./components/core";
export default function App() {
	return (

		<Core>
			<Router
				root={(props) => (
					<>
						<main id="MAIN" class="text-center mx-auto">
							<Header />


							<Suspense fallback={<Loading />}>

								<Content>
									<div id="HTML">
										{props.children}
									</div>
								</Content>
							</Suspense>

						</main>
					</>
				)}
			>
				<FileRoutes />
			</Router>

		</Core>
	);
}