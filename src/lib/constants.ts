import { getContent } from "~/server/sanity";


// Try to keep a standardized max size for all image assets. Assets should scale down from here as needed.
export const IMAGE_WIDTH = 1024;
export const IMAGE_HEIGHT = 768;

export const IMAGE_LARGE_WIDTH = 1280
export const IMAGE_LARGE_HEIGHT = 920

// helpers to figure out what kind of asset an image might be
export const MAIN_TAG = "main";
export const ASSET_TAG = "asset";

/////// QUERY KEYS //////
export const SANITY_KEY = "SanityContent";
export const CONTENT_OPTS = {
	experimental_prefetchInRender: true,
	queryKey: [SANITY_KEY],
	queryFn: async () => await getContent()
}