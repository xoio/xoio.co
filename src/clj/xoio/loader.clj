
(ns xoio.loader
  (:require
    [garden.stylesheet :refer [at-media]]
    [garden.units :refer [px]]
    [garden.def :refer [defstylesheet defstyles]]))

;; defines the style for the loader
(def LoaderLayout
  [:#LOADER {
             :height (px 5)
             :background "red"
             :position "absolute"
             :top  0
             }
   [:&.hide {:opacity 0
             :transition "all 0.5s ease"}
    ]])
