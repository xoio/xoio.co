(ns xoio.styles
  (:require
    [xoio.vars :as vars]
    [xoio.project :as project]
    [xoio.loader :as loader]
    [xoio.typography :as type]
    [xoio.menu :as menu]
    [garden.stylesheet :refer [at-media]]
    [garden.units :refer [px]]
    [garden.def :refer [defstylesheet defstyles]]))

(defstyles
  screen
  [:* {:margin 0
       :padding 0}


   [:body {:background-color "#f5f5f5"}]

   [:#SITE {:left "50%"
            :top "50%"
            :position "absolute"
            :overflow "hidden"
            :overflow-y "scroll"
            :background "#d1d1d1"
            :opacity 0
            :transition "opacity 0.5s ease"
            :transform "translateX(-50%) translateY(-50%)"
            }

    [:&.show {:opacity 1
              :transition "opacity 0.5s ease"}]
    ]
   [:header {:z-index 999
             :opacity 0
             :position "relative"
             :padding vars/padding}

    ;; for when we're hoving over a project name in the home page
    ;; TODO need to come up with a more all round color that'll show up better
    [:&.project-preview {:color "#fff"}]

    [:.header-overlay {:background "rgba(255,255,255,0.5)"}]
    [:&.show {:opacity 1
              :transition "opacity 1.9s ease"}]]

   [:#MENU {:position "absolute"
            :width "100%"
            :height "100%"
            :top 0
            :left "-100%"
            :z-index 96
            :background "red"}]

   [:#HOME {:position "absolute"
            :bottom "0"
            :padding "40px"
            :padding-bottom "100px"}]

   [:div#project-preview {:position "absolute"
                          :width "100%"
                          :height "100%"
                          :z-index "-1"
                          :background "red"}]

   [:div.project-item {}
    [:h2 {:padding 0
          :transition "all 0.5s ease"
          :background-color "transparent"}
     [:&.not-mobile:hover {:cursor "pointer"
                :background-color "red"
                :transition "all 0.5s ease"
                :padding "4px"}]]]
   ]

  loader/LoaderLayout
  type/HeaderTypography
  type/MainTypography
  type/NavTypography
  menu/MenuStyles
  project/projectHome
  project/projectPreviewLinks
  project/projectPage
  )