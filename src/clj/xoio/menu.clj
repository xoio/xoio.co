(ns xoio.menu
  (:require
    [xoio.vars :as vars]
    [garden.def :refer [defstylesheet defstyles]]))


(def MenuStyles
  [:#MENU {:font-size "30px"}

   [:#SOCIAL {:margin-top "20px"}]
   [:nav {:margin vars/padding
          :border-top "solid 5px #333"
          :padding-top "20px"
          :width "200px"
          :position "relative"
          :top "50px"}]

   [:div {}
    [:&:hover {:cursor "pointer"}]]
   ])
