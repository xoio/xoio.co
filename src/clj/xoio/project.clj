(ns xoio.project
  (:require
    [xoio.vars :as vars]
    [garden.def :refer [defstylesheet defstyles]]))


(def projectHome
  [:#PROJECT_PREVIEW {:width "100%"
                      :padding-top "20px" }

   [:img {:opacity 0
          :position "absolute"
          :top "50%"
          :left "50%"
          :transform "translateX(-50%) translateY(-50%)"
          :width "97%"
          :transition "opacity 0.5s ease"}
    [:&.show-preview {:transition "opacity 0.5s ease"
                      :opacity 1}]]])


(def projectPreviewLinks
  [:#HOME
   [:.project-item {:transition "all 0.5s ease"
                    :opacity 1}
      [:&.hide {:transition "all 0.5s ease"
                :opacity 0}]
    ]

   ])


(def projectPage
  [:#PROJECTS {:position "relative"
               :width "100%"
               :background "red"
               :padding "20px"
               :padding-top "400px"
               :margin-top "120%"
               :height "100%"
               }
   [:div.project-wrap {:background "#fff"
                       :width "100%"
                       :padding-bottom "50px"
                       :height "100%"}

    [:.project-title [:div {:padding vars/project-padding}]]
    [:.project-copy-container {:padding vars/project-padding
                     :display "flex"}
     [:.project-copy {:margin-right "10px"}]
     ]


    ]])