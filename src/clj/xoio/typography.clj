(ns xoio.typography
  (:require
    [garden.def :refer [defstylesheet defstyles]]))




(def MainTypography
  [:body {
          :font-family "brandon-grotesque,sans-serif"
          }])

;; define the typography styles for the header
(def HeaderTypography
  [:header {
         :font-size "98%"
         :letter-spacing "1px"
         :font-weight 400
         }
   [:h2 {:font-size "14px"
         :letter-spacing "2px"}]]
  )

;; define typography for navigation
(def NavTypography
  [:nav
   [:div.nav-link {
                   :font-size "12px"
                   :letter-spacing "2px"
                   :padding-top  "5px"
                   :text-transform "uppercase"
                   }]])
