import { createClient } from "@sanity/client"
import ImageBuilder from "@sanity/image-url"


export const client = createClient({
	projectId: "zhfj9ukd",
	dataset: "production",
	apiVersion: '2021-08-31',
	useCdn: true
})

export const image_builder = ImageBuilder(client)

// Gets content from Sanity. Also parses out image urls 
// and gets stuff set up to be preloaded. 
export const getContent = async () => {
	"use server";

	const content = await client.fetch(`*[_type == "projects"]`)
	const projects = content.map((proj: any) => {

		let tmp = { ...proj }

		// Setup the main / thumbnail image 
		const main = image_builder.image(proj.mainImage).width(1024).height(768).url()
		const mainLarge = image_builder.image(proj.mainImage).url()
		tmp.mainImage = {
			id: proj._id,
			small: main,
			large: mainLarge
		}


		// setup the other project assets 
		const projAssets = proj.photos.map((itm: any) => {

			const asset = image_builder.image(itm.asset).width(1024).height(768).url()
			const largetAsset = image_builder.image(itm.asset).width(1024).height(768).url()

			return {
				id: proj._id,
				small: asset,
				large: largetAsset
			}


		})

		tmp.photos = projAssets
		return tmp
	})

	return projects

}

/*
	
		// parse out the final url for the assets 
	const projects = content.map((proj: any) => {
		let tmp = { ...proj }

		// handle the main image
		tmp.mainImage = {

			id: proj._id,
			url: image_builder.image(proj.mainImage)
				.width(1024)
				.height(768)
				.url()
		}

		tmp.photos = tmp.photos.map((asset: any) => {
			return {
				id: proj._id,
				url: image_builder.image(asset)
					.width(1024)
					.height(768)

			}
		})
		return tmp
	})

*/