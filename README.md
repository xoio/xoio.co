# xoio.co

This is my portfolio. Always under development. 

I did have another repo with the old version as well as several explorations; you can find that [here](https://gitlab.com/xoio/xoio.co.explorations/).

Setup
===
* `npm install` to install dependencies
* `npm run dev` to start the dev server. 