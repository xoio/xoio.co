(set-env!
 :source-paths    #{"src/cljs" "src/clj"}
 :resource-paths  #{"resources"}
 :dependencies '[[adzerk/boot-cljs          "1.7.48-6"   :scope "test"]
                 [adzerk/boot-cljs-repl     "0.2.0"      :scope "test"]
                 [adzerk/boot-reload        "0.4.1"      :scope "test"]
                 [pandeiro/boot-http        "0.6.3"      :scope "test"]
                 [org.clojure/clojurescript "1.7.122"]
                      [org.clojure/tools.reader "1.0.0-alpha2"]
                                  [thi.ng/domus "0.2.0"]
                                  [hipo "0.5.1"]
                                  [garden "1.3.0-SNAPSHOT"]
                                  [thi.ng/geom "0.0.908"]
                 [org.martinklepsch/boot-garden "1.2.5-3" :scope "test"]])

(require
 '[adzerk.boot-cljs      :refer [cljs]]
 '[adzerk.boot-cljs-repl :refer [cljs-repl start-repl]]
 '[adzerk.boot-reload    :refer [reload]]
 '[pandeiro.boot-http    :refer [serve]]
 '[org.martinklepsch.boot-garden :refer [garden]])

(deftask build []
  (comp
        
        (cljs)
        
        (garden :styles-var 'xoio.styles/screen
:output-to "css/garden.css")))

(deftask run []
  (comp (serve)
        (watch)
        (cljs-repl)
        (reload)
        (build)))

(deftask production []
  (task-options! cljs {:optimizations :advanced
  :externs ["resources/js/externs.js"]}
                      garden {:pretty-print false})
  identity)

(deftask development []
  (task-options! cljs {:optimizations :none :source-map true}
                 reload {:on-jsload 'xoio.app/init})
  identity)


(deftask prod
  "Simple alias to run application in development mode"
  []
  (comp (production)
        (run)))


(deftask dev
  "Simple alias to run application in development mode"
  []
  (comp (development)
        (run)))


