const IMAGE_EVENT = "IMAGE_LOADED";
const STATUS_EVENT = "STATUS_EVENT";

onmessage = async (e) => {
	let images = e.data;
	let totalSize = 0;
	let imageBytesLoaded = 0;

	for (const itm of images) {
		await fetch(itm.url).then(async (res) => {
			let blob = await res.blob();
			totalSize += blob.size;
		});
	}

	for (const itm of images) {
		const res = await fetch(itm.url);
		const reader = res.body.getReader();
		const chunks = [];

		while (true) {
			const { done, value } = await reader.read();
			if (done) {
				const blob = new Blob(chunks);
				const url = URL.createObjectURL(blob);
				const bitmap = await createImageBitmap(blob);

				postMessage({
					event: IMAGE_EVENT,
					id: itm.id,
					tag: itm.tag,
					url,
					bitmap,
				});
				break;
			} else {
				chunks.push(value);
				imageBytesLoaded += value.length;
				loadPct = Math.floor((imageBytesLoaded / totalSize) * 100);
				postMessage({
					event: STATUS_EVENT,
					pct: loadPct,
				});
			}
		}
	}
};

/*

    let images = e.data

    let numImages = images.length
    let loaded = 0
    let totalSize = 0
    let imageBytesLoaded = 0
    let loadPct = 0

    ////////// GET TOTAL SIZE OF IMAGES TO LOAD ////////////
    for (const itm of images) {
        await fetch(itm.url).then(res => {
            totalSize += parseInt(res.headers.get('Content-Length'))
        })
    }


    /////// START LOADING IMAGES /////////
    for (const itm of images) {

        const res = await fetch(itm.url)

        const reader = res.body.getReader();
        const chunks = [];
        while (true) {
            const {done, value} = await reader.read();

            if (done) {
                const blob = new Blob(chunks);
                const url = URL.createObjectURL(blob);

                postMessage({
                    event: IMAGE_EVENT,
                    id: itm.id,
                    tag: itm.tag,
                    url
                })
                loaded += 1;
                break
            } else {
                chunks.push(value);
                imageBytesLoaded += value.length;
                loadPct = Math.floor((imageBytesLoaded / totalSize) * 100)

                //  console.log(loadPct)
                postMessage({
                    event: STATUS_EVENT,
                    pct: loadPct,
                    id: itm.id
                })
            }
        }
    }
 */