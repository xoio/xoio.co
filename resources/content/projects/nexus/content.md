<div class="intro project-copy">
__Introduction__
<br/>
This was a project done while working at Unit9. The San Francisco branch had a google account
and was tasked with finishing up the new Nexus Website.
</div>

<div class="technicals project-copy">

__Technicals__
<br/>
This was a basically a pretty straightforward Angular.js website detailing the new Nexus 5 as well as
featuring the Nexus 10 and 7.
<br/>
<br/>

I was responsible primarily with coming up with a pseudo media player(in that it really only 
played one video) as well as later on moving onto dealing with Localization of the site
which ended up being translated into well over 15 languages.
</div>