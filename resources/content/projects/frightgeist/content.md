<div class="intro project-copy">
__Introduction__
<br/>

I was brought onto this project by the good folks at [UseAllFive](http://www.useallfive.com/) to help out with some of the WebGL work 
necessary to help bring the map to life. I helped build out the foundational portions of the map including setting up the geometry and meshes to 
create the particle system that the map comprises, in addition to doing additional work in integrating the various other components necessary
to get the map to react the way it does. 

<br/>
In addition to all of that I build out a couple of other smaller visual things like the dripping effect on a costume page as well as the 
spider web that appears on the state summary pages. 
</div>

<div class="technicals project-copy">

__Technicals__
<br/>

    The work done was fairly straightforward. For the map I explored a couple of different approaches; at first I made a attempt to integrate
    a physics engine but soon realized that, given the size of the dataset, that having a engine as well as keeping things performant
    would soon prove to be a extreme challenge. That being said we settled on a much simpler particle system integrating Jono Brandel's port
    of the [Traer physics's library](http://jonobr1.github.io/Physics/) to help build out the animation. 
    <br/>
    <br/>
    The particles are built out of complete meshes, with shared geometry and materials as opposed to simple points as one might normally do with a 
    particle system. While using points did indeed work fine in terms of visual appearance, we soon discovered, that, unfortunately, in spite of visually
    appearing the correct size, that the hit area for Raycasting was sadly, stuck to the size of the point which is roughly 1 pixel, increasing the pixel 
    size in the vertex shader oddly didn't appear to help :( 
    <br/>
    <br/>
    Moving on, for the drips, I turned to another one of Jono's projects [Two.js](https://jonobr1.github.io/two.js/) as I am terrible at ploting out
    bezier curves and wanted something that could look nice at any resolution. To get the curves, I read a existing svg path into the Two.js instance I created, 
    and from there, was able to manipulate the path like I would vertices in Three.js.
    <br/>
    <br/>
    For the spiderweb, I ported a example from [Verlet-js](https://github.com/subprotocol/verlet-js) as it basically already did everything we needed it to do. 
    For the most part, the sample is exactly the same, but re-built a bit in order to allow it to resize and be responsive. The library was also modified a bit in order
    to remove some of the drawing commands as well as enable the ability to choose colors a bit eaiser.
</div>