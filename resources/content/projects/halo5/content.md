<div class="intro project-copy">
__Introduction__
<br/>

Around the start of the summer, I was asked to participate in the development of a interactive experience for Microsoft. This would have been a experience for the 
recently release video game, Halo 5 : Guardians. I was part of a team that put together a rpg/choose your own adventure-like game. I won't spoil any of the story, but more 
or less, the game imparted some of the backstory surrouding the events leading up to the Halo 5 story.

Sadly though, within a couple of weeks of the scheduled launch, the project got axed (◕︵◕)
</div>

<div class="technicals project-copy">

__Technicals__
<br/>
The bulk of the site is comprised of a custom framework utilizing Twitter's FlightJs. I helped put together a couple components 
that governed the UI as well as integration of audio and other misc. things. I also wrote the initial draft for the console tool 
in the main game itself. 

In addition to all of that, I got to do a little WebGL/Canvas2D work (which was handled using Three.js and Pixi.js) related work by writing a GLSL shader or two that was integrated into the 
main story.

</div>