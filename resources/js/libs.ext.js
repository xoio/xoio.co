/**
 *  Extern notes -
 *  1. FIX ALL WARNINGS - they still cause problems
 *  2. You only need to declare functions you use
 */

/// Declarations for Preloadjs
var createjs = {}
createjs.LoadQueue = function(){}
createjs.on = function(a,b,c,d,e,f){}
createjs.loadManifest = function(a){}

/// Declarations for TweenMax
var TweenMax = {}
TweenMax.staggerFrom = function(a,b,c,d){}
TweenMax.to = function(a,b,c,d){}

/// Declarations for Page.js
var page = function(a,b){}
var page = function(a){}
page.redirect = function(a,b){}
page.stop = function(){}
page.exit = function(a,b){}
page.start = function(){}

/// Declarations for Typekit
var Typekit = {}
Typekit.load = function(a){}


// Threejs declarations but will probably just write needed stuff as a JS lib
var THREE = {}
THREE.WebGLRenderer = function(a){}
THREE.Scene = function(){}
THREE.PerspectiveCamera = function(a,b,c,m){}
THREE.BufferGeometry = function(){}
THREE.ShaderMaterial = function(a){}
THREE.RawShaderMaterial = function(a){}
THREE.DataTexture = function(a){}
THREE.CSS3DRenderer = function(){}
THREE.CSS3DRenderer.setSize = function(a,b){}
THREE.CSS3DRenderer.render = function(scene,camera){}